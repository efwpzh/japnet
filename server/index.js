const { Server } = require("ws");
const app = require("express")();
const { static } = require("express");
const shortid = require("shortid");
const { PPS } = require("./www/shared/globalVariables.js");

const wss = new Server({ noServer: true });

const basePositions = {
	main: [1 * 50 + 25, 6 * 50 + 25],
	alt:  [1 * 50 + 25, 8 * 50 + 25],
};

const baseRooms = {
	main: [0, 8],
	alt:  [0, 0]
};

const positions = {};
const names = {};
const requests = {};
let messagesThisSecond = {};

function make(k, v) {
	return JSON.stringify({ k, v })
}

function nameToUID(name) {
	for (const uid in names) {
		if (names[uid] === name) return uid;
	}
	return false;
}

wss.on("connection", ws => {
	ws.id = shortid.generate();
	ws.on("message", data => {
		let obj = {};
		try {
			obj = JSON.parse(data);
		} catch (e) {}
		switch (obj.k) {
			case "positions":
				const [x, y, lc1, lc2, grav] = obj.v;
				if (
					isNaN(x) ||
					isNaN(y) ||
					isNaN(lc1) ||
					isNaN(lc2) ||
					!!grav !== grav
				)
					return;

				positions[ws.id] = obj.v;
				break;

			case "welcome": {
				if (names[ws.id]) break;
				let { name } = obj.v;
				// hardcap at 32
				if (typeof name !== "string" || name === "") name = "anon";
				name = name.split(" ").join("").substring(0, 32);

				let isDuplicate = false;
				Object.values(names).forEach(nm => {
					if (nm === name) isDuplicate = true;
				});
				if (isDuplicate || name.match(/[^a-z0-9_-]/iu)) {
					ws.send(make("reload", 0));
					ws.terminate();
					break;
				}

				positions[ws.id] = [0, 0, 0, 0];
				names[ws.id] = name;

				wss.clients.forEach(client => {
					client.send(make("names", names));
				});

				wss.clients.forEach(client => {
					client.send(make("chat", `${name} joined the server`));
				});

				ws.send(
					make(
						"chat",
						"Welcome to JAPNet! Type '/help' for a list of commands."
					)
				);
				break;
			}

			case "chat": {
				if (typeof obj.v !== "string") return;
				if (obj.v == "") return;
				if (obj.v.length > 1000) return;

				messagesThisSecond[ws.id] = (messagesThisSecond[ws.id] ?? 0) + 1;
				if (messagesThisSecond[ws.id] > 5) {
					ws.send(make("reload", 0))
				} else if (obj.v.startsWith("/")) {
					let args = obj.v.substring(1).split(" ");
					switch (args[0]) {
						case "help":
							ws.send(make("chat", "there's just /tpa lol"));
							break;
						case "cmpgn":
							if (!basePositions[args[1]]) break;
							ws.send(
								make("tp", [
									basePositions[args[1]][0],
									basePositions[args[1]][1],
									baseRooms[args[1]][0],
									baseRooms[args[1]][1],
									325,
									1,
									600,
									args[1],
								])
							);
							break;
						case "tpa": {
							const uid = nameToUID(args[1]);
							if (uid === ws.id) {
								ws.send(make("chat", "that's you"))
							} else if (uid) {
								let sent = false;
								wss.clients.forEach(client => {
									if (client.id === uid) {
										client.send(
											make(
												"chat",
												`${names[ws.id]} wants to teleport to you! (/tpaccept)`
											)
										);
										sent = true;
										requests[client.id] = ws.id;
									}
								});
								if (sent)
									ws.send(make("chat", "request sent!"));
								else
									ws.send(
										make("chat", "something went wrong")
									);
							} else ws.send(make("chat", "never heard of em"));
							break;
						}
					}
				} else {
					wss.clients.forEach(client => {
						client.send(
							make(
								"chat",
								`${names[ws.id]}: ${obj.v.substring(0, 1000)}`
							)
						);
					});
				}
				break;
			}
			case "tpaccept": {
				const to = [
					positions[ws.id][0],
					positions[ws.id][1],
					positions[ws.id][2],
					positions[ws.id][3],
					...obj.v,
				];
				if (requests[ws.id]) {
					wss.clients.forEach(client => {
						if (client.id === requests[ws.id]) {
							client.send(make("tp", to));
							delete requests[ws.id];
						}
					});
				} else ws.send(make("chat", "what"));
				break;
			}
		}
	});
	ws.on("close", () => {
		wss.clients.forEach(client => {
			client.send(
				make("goodbye", ws.id)
			);
		});

		wss.clients.forEach(client => {
			client.send(
				make("chat", `${names[ws.id]} left the server`)
			);
		});
		delete positions[ws.id];
		delete names[ws.id];
		ws.terminate();
	})
});

const server = app.listen(8000);

server.on("upgrade", (request, socket, head) => {
	wss.handleUpgrade(request, socket, head, socket => {
		wss.emit("connection", socket, request);
	});
});

setInterval(() => {
	wss.clients.forEach(client => {
		let tmp = { ...positions };
		delete tmp[client.id];
		client.send(make("positions", tmp));
	});
}, 1000 / PPS);

setInterval(() => {
	messagesThisSecond = {};
}, 1000);

app.use(static("www"));