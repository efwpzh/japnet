let lastSent = [0, 0];
let name;
let meta = {};

const ws = new WebSocket(`wss://${location.host}/`);

ws.onopen = evt => {
	name = prompt("Enter username:");
	color = getColor(name);
	drawPlayer();

	send("welcome", { name });
};

ws.onmessage = evt => {
	const obj = JSON.parse(evt.data);
	const data = obj.v;
	switch (obj.k) {
		case "positions":
			positions = data;
			drawPlayer();
			break;
		case "names":
			names = data;
			drawPlayer();
			break;
		case "tp":
			player.x = data[0];
			player.y = data[1];
			player.levelCoord = [data[2], data[3]];
			player.g = data[4];
			player.maxJumps = data[5];
			player.moveSpeed = data[6];
			player.map = data[7];

			worldMap = worldMaps[player.map];
			levels = metaLevels[player.map];
			drawLevel();
			break;
		case "chat": {
			let el = document.createElement("p");
			el.textContent = data;
			id("chat").appendChild(el);

			id("chat").scrollTo(0, id("chat").scrollHeight);
			break;
		}
		case "goodbye":
			delete positions[data];
			delete names[data];
			break;
		case "reload":
			alert("That name is taken or invalid, try another one!");
			window.location.reload();
			break;
	}
};

window.setInterval(() => {
	if (lastSent[0] === player.x && lastSent[1] === player.y) return;

	lastSent = [player.x, player.y, player.levelCoord[0], player.levelCoord[1], player.g >= 0,player.map];
	send("positions", lastSent);
}, 1000 / PPS);

function send(k, v) {
	// mm yes very good drop the packet
	if (ws.readyState !== 1) return;
	ws.send(JSON.stringify({ k, v }));
}

function sendChatMessage(msg) {
	if (msg === "/tpaccept")
		send("tpaccept", [player.g, player.maxJumps, player.moveSpeed, player.map]);
	else send("chat", msg);
}

setInterval(() => {
	if (player.levelCoord[0] === 5 && player.levelCoord[1] === 4) drawLevel();
}, 1000);

function showPlayerList() {
	id("players").style.display = "block";
	id("playersList").innerHTML = "";
	// update list
	let players = 0;
	for (const uid in names) {
		players++;
		const name = names[uid];
		const nameWrapper = document.createElement("div");
		nameWrapper.classList.add("playerWrapper")
		const playerIcon = document.createElement("div");
		playerIcon.classList.add("player");
		playerIcon.style.backgroundColor = checkColorCache(uid);
		nameWrapper.appendChild(playerIcon);
		const playerName = document.createElement("span");
		playerName.textContent = name;
		nameWrapper.appendChild(playerName);
		id("playersList").appendChild(nameWrapper);
	}

	id("playersOnline").textContent = players
	id("playersOnlinePlural").textContent = (players === 1 ? "" : "s")
}

function hidePlayerList() {
	id("players").style.display = "none";
}

// CCC OOO L   OOO RRR
// C   O O L   O O RR
// CCC OOO LLL OOO R R

String.prototype.hashCode = function () {
	var hash = 0;
	for (var i = 0; i < this.length; i++) {
		var character = this.charCodeAt(i);
		hash = (hash << 5) - hash + character;
		hash = hash & hash; // Convert to 32bit integer
	}
	return hash;
};

function componentToHex(c) {
	var hex = c.toString(16);
	return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
	return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

function getColor(string) {
	if (string == undefined) string = "failsafe"
	let hash = string.hashCode();
	let r = (hash & 0xff0000) >> 16;
	let g = (hash & 0x00ff00) >> 8;
	let b = hash & 0x0000ff;

	return rgbToHex(r, g, b);
}
